FROM node:alpine
WORKDIR /app
COPY package*.json ./
RUN npm install
COPY index.js ./
EXPOSE 80
EXPOSE 4222
ENTRYPOINT ["node", "index.js"]
CMD ["--target", "localhost:4222", "--port", "80"]
