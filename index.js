const net = require('net');
const WebSocket = require('ws');

const getArguments = () => {
  try {
    const argv = process.argv.slice(2);
    const args = {};
    for (let i=0; i<argv.length; i++) {
      const arg = argv[i];
      if (arg.indexOf('--') !== 0) continue;
      const next = argv[i+1];
      if (next && next.indexOf('--') !== 0) {
        args[arg.replace('--', '')] = next;
        i++;
      }
    }

    return {
      target: args.target || 4222,
      targetHost: args.targetHost || 'localhost',
      port: args.port || 80,
    };
  } catch (e) {
    console.log(e.message);
    process.exit();
  }
};

const { port, target, targetHost } = getArguments();
const ws = new WebSocket.Server({ port });
ws.on('connection', webSocket => {
  const tcpSocket = net.connect(target, targetHost);

  tcpSocket.on('data', data => {
    webSocket.send(data.toString('utf8'));
    console.log(data.toString('utf8'));
  });
  // tcpSocket.on('data', data => webSocket.send(data, { binary: true }));
  webSocket.on('message', data => {
    tcpSocket.write(data);
    console.log(data);
  });

  tcpSocket.on('close', () => {
    webSocket.removeAllListeners('close');
    webSocket.close();
  })
  webSocket.on('close', () => {
    tcpSocket.removeAllListeners('close');
    tcpSocket.end();
  });

  tcpSocket.on('error', e => {
    console.log(e);
    tcpSocket.end();
  });
  webSocket.on('error', e => {
    console.log(e);
    webSocket.close();
  });
});
